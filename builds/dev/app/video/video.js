(function() {
	'use strict';

	angular
		.module('ang.video', [])
		.controller('VideoController', VideoController);

	/** @ngInject */
	function VideoController($stateParams, Y) {
		var vm = this;
		vm.channelName = $stateParams.channelName;
		vm.playlist = $stateParams.playlist;
		vm.videoId = $stateParams.videoId;
		vm.comments = {};
		activate();

		function activate() {
			getComments(vm.videoId);
		}
		
		function getComments(id){
			return Y.getComments(id).then(function(response){
				vm.comments = response.items;
				return vm.comments;
			})
		}
		
	}
})();
