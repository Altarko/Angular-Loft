(function() {
	'use strict';

	angular
		.module('ang.channel', [])
		.controller('ChannelController', ChannelController);

	/** @ngInject */
	function ChannelController($stateParams, Y) {
		var vm = this;
		vm.channelName = $stateParams.channelName;
		vm.playlists = [];
		activate();

		function activate() {
			console.log(vm.channelName);
			getChannelId(vm.channelName).then(function (channelId) {
				getPlaylists(channelId);
			});
			//
		}
		
		function getPlaylists(id) {
			return Y.getPlaylists(id).then(function(response) {
				vm.playlists = response.items;
				//console.log(vm.playlists);
				return vm.playlists;
			})
		}

		function getChannelId(name) {
			return Y.getChannelId(name).then(function(response) {
				return response.items[0].id;
			})
		}
		
	}
})();
