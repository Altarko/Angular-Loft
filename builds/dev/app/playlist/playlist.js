(function() {
	'use strict';

	angular
		.module('ang.playlist', [])
		.controller('PlayListController', PlayListController);

	/** @ngInject */
	function PlayListController($stateParams, Y) {
		var vm = this;
		vm.channelName = $stateParams.channelName;
		vm.playlist = $stateParams.playlist;
		vm.videos = {};
		activate();

		function activate() {
			getVideos(vm.playlist);
		}
		
		function getVideos(id){
			return Y.getVideos(id).then(function(response){
				vm.videos = response.items;
				return vm.videos;
			})
		}
		
	}
})();
