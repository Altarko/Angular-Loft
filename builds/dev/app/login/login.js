(function() {
	'use strict';

	angular
		.module('ang.login', [])
		.controller('LoginController', LoginController);

	/** @ngInject */
	function LoginController() {
		var vm = this;
		
		vm.fakeData = {
			username: 'demo',
			password: 'demo'
		};

		activate();

		function activate() {
			console.log('LoginCtrl');
		}
		
	}
})();
