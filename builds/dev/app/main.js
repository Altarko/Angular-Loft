(function(){
  'use strict';

  angular
  	.module('ang',[
        'ui.router',
        'ui.bootstrap',
  	    'ang.home',
        'ang.users',
		'ang.login',
		'ang.dash',
		'ang.channel',
		'ang.playlist',
		'ang.video',
		'ang.youtubeservice'
  	  ])
  	.config(MainConfig)
		.run(MainRun)
  	
  	function MainConfig($urlRouterProvider, $logProvider, $stateProvider)
  	{
      $logProvider.debugEnabled(false);

		$stateProvider
			.state('login', {
				url: '/login',
				templateUrl: 'app/login/login.html',
				controller: 'LoginController',
				controllerAs: 'lc'
			})
			.state('dashboard', {
				url: '/dashboard',
				templateUrl: 'app/dashboard/dashboard.html',
				controller: 'DashController',
				controllerAs: 'dc'
			})
			.state('dashboard.channel', {
				url: '/channel/:channelName/',
				templateUrl: 'app/channel/channel.html',
				controller: 'ChannelController',
				controllerAs: 'cc'
			})
			.state('dashboard.video', {
				url: '/channel/:channelName/:playlist',
				templateUrl: 'app/playlist/playlist.html',
				controller: 'PlayListController',
				controllerAs: 'pc'
			})
			.state('dashboard.video-comments', {
				url: '/channel/:channelName/:playlist/:videoId',
				templateUrl: 'app/video/video.html',
				controller: 'VideoController',
				controllerAs: 'vc'
			});
      $urlRouterProvider.otherwise('/login');
  	}
  	
  	function MainRun($log)
  	{
  		$log.debug('MainRun');
  	}

})();


// Singleton — одиночка — 
// только в одном экземпляре может одновременно существовать

// var Singleton;
// Singleton = (function(){
//   var instance; 

//   instance = {
//     count: 0
//   };

//   return function(){
//     return instance;
//   }
// }());

// var s1 = new Singleton();
// console.log('S1: ', s1.count);
// var s2 = new Singleton();
// console.log('S2: ', s2.count);

// s1.count++;
// console.log('S1: ', s1.count);
// console.log('S2: ', s2.count);
// s2.count+=2;
// console.log('S1: ', s1.count);
// console.log('S2: ', s2.count);