;(function(){
  'use strict';

  angular
  	.module('ang.home', [
        'ang.users',
      ])
  	.controller('HomeCtrl', HomeController)
  	.controller('ChildCtrl', ChildController)
  	.config(HomeConfig)
		.run(HomeRun)
    



  	
  	function HomeConfig($stateProvider)
  	{
  	  
  	}
  	
  	function HomeRun() 
  	{
  		// console.log('HomeRun');
  	}

  	/**
  	 * Child Controller
  	 */
  	function ChildController()
  	{
  	  var s = this;
  	  s.message = 'Child message';
  	}
  	
	  /**
	   * Контроллер главной страницы
	   */
    // function HomeController ($scope, $log, FURL, user, fakeconst, usersServ, usersFact, pusers) {
	  function HomeController ($scope, $log) {
  	  $log.init('HomeCtrl');
      var s = this;
	  	s.message = 'Привет, ';
	  	s.name = '';

	  	s.hello = function(){
	  		s.message = 'Привет, ' + s.name + '!';
	  	}
      // $log.debug('HomeCtrl ', FURL);
      // FURL = '123';
      // $log.debug('HomeCtrl ', FURL);

      // $log.debug('HomeCtrl ', user);
      // user.id = Math.random();
      // $log.debug('HomeCtrl ', user);

      // $log.debug('HomeCtrl ', fakeconst);
      // fakeconst.id = Math.random();
      // $log.debug('HomeCtrl ', fakeconst);

      // $log.debug('HomeCtrl ', usersServ.get());
      // $log.debug('HomeCtrl ', usersServ.inc());
      // $log.debug('HomeCtrl ', usersFact.get());
      // $log.debug('HomeCtrl ', usersFact.inc());
      // $log.debug('HomeCtrl ', ++pusers.id);
      // $log.debug('HomeCtrl ', pusers.getConfig());




	  } /* HomeController */
  
})();
