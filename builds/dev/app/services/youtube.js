/**
 * Сервис для работы с Тютюбом
 */

(function () {
  'use strict';

  angular
    .module('ang.youtubeservice', [])
    .factory('Y', Y);

  /** @ngInject */
  function Y($http) {

    var key = 'AIzaSyC1lhZw1t3euNILruHP5d-XdrA812sqBTk';

    var service = {
      getVideos: getVideos,
      getChannelId: getChannelId,
      getPlaylists: getPlaylists,
      getComments: getComments
    };

    return service;

    /**
     * Запрашивает видюшки из плейлиста
     * @returns {*}
     */
    function getVideos(id) {
      return $http({
        method: 'GET',
        url: 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet' + '&playlistId=' +  id + '&key=' + 'AIzaSyC1lhZw1t3euNILruHP5d-XdrA812sqBTk'
      })
        .then(getZonesComplete)
        .catch(getZonesFailed);

      // если запрос прошел успешно
      function getZonesComplete(response) {
        return response.data;
      }

      // если запрос не прошел
      function getZonesFailed(error) {
        console.log('Запрос зон', error.data, 'Ошибка');
      }
    }

    /**
     * Запрашивает плейлисты
     * @returns {*}
     * @comments https://developers.google.com/youtube/v3/docs/playlists/list#parameters
     */
    function getPlaylists(channelId) {
      var part = 'snippet',
          maxResults = 50;
      return $http({
        method: 'GET',
        url: 'https://www.googleapis.com/youtube/v3/playlists?part=' + part + '&channelId=' + channelId + '&maxResults=' + maxResults + '&key=' + key
      })
          .then(getPlaylistsComplete)
          .catch(getPlaylistsFailed);

      // если запрос прошел успешно
      function getPlaylistsComplete(response) {
        return response.data;
      }

      // если запрос не прошел
      function getPlaylistsFailed(error) {
        console.error('Ошибка getPlaylists' + error.data);
      }
    }

    /**
     * Узнаем id канала
     * @returns {*}
     * @comments https://developers.google.com/youtube/v3/docs/channels/list#http-request
     */
    function getChannelId(channelName) {
      var part = 'id';
      return $http({
        method: 'GET',
        url: 'https://www.googleapis.com/youtube/v3/channels?part=' + part + '&forUsername=' + channelName + '&key=' + key
      })
          .then(getChannelIdComplete)
          .catch(getChannelIdFailed);

      // если запрос прошел успешно
      function getChannelIdComplete(response) {
        return response.data;
      }

      // если запрос не прошел
      function getChannelIdFailed(error) {
        console.error('Ошибка getPlaylists' + error.data);
      }
    }

    /**
     * Запрашивает коммменты в видюшке
     * @returns {*}
     * @comments https://developers.google.com/youtube/v3/docs/comments/list#http-request
     */
    function getComments(id) {
      var part = 'snippet',
          maxResults = 50;
      return $http({
        method: 'GET',
        url: 'https://www.googleapis.com/youtube/v3/commentThreads?part=' + part + '&videoId=' + id + '&maxResults=' + maxResults + '&key=' + key
      })
          .then(getCommentsComplete)
          .catch(getCommentsFailed);

      // если запрос прошел успешно
      function getCommentsComplete(response) {
        return response.data;
      }

      // если запрос не прошел
      function getCommentsFailed(error) {
        console.error('Ошибка getComments' + error.data);
      }
    }


  }
})();
