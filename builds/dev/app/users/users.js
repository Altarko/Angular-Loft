;(function () {
    'use strict';

    angular
        .module('ang.users', [])
        .config(UsersConfig)
        .controller('UsersCtrl', UsersController)
        .factory('users', usersFactory)
        .filter('orderKeyName', orderKeyNameFilter)
        .filter('ageOnlyMale', ageOnlyMaleFilter)
    // // Constant
    //   .constant('FURL', 'http://some-page-url.ru/')
    //   .constant('fakeconst', {'id': null})
    //   // Value
    //   .value('user', {
    //   	'id': null,
    //   	'fullname': null
    //   })
    // // Service
    // .service('usersServ', usersServ)
    // // Factory
    // .factory('usersFact', usersFact)
    // // Provider
    // .provider('pusers', pusersProviderFunction)


    // function usersServ() {
    // // private data and functions
    // var count = 0;

    // // public data and functions
    // this.get = function(){
    // 	return count;
    // } /* get */

    // this.inc = function(){
    // 	return ++count;
    // } /* inc */
    // } /* usersServ */

    // function usersFact() {
    // 	var o = {};
    // // private data and functions
    // var count = 0;

    // // public data and functions
    // o.get = function(){
    // 	return count;
    // } /* get */

    // o.inc = function(){
    // 	return ++count;
    // } /* inc */

    // return o;
    // } /* usersServ */

    // function pusersProviderFunction(){
    // 	// config data
    // 	var configVal = 'slovo';
    // 	// provider config stuff
    // 	return {
    // 		setConfigVal : function(_val){
    // 			configVal = _val;
    // 		},
    // 		getConfigVal : function(){
    // 			return configVal;
    // 		},
    // 		$get: function(){
    // 			// provider run staff
    // 			function getVal(){
    // 				// setConfigVal('asd'+Math.random()); // ReferenceError: setConfigVal is not defined
    // 				return configVal;
    // 			}

    // 			return {
    // 				id: 0,
    // 				getConfig: getVal
    // 			}
    // 		}
    // 	}
    // } /* pusersProviderFunction */

    function ageOnlyMaleFilter() {
        return function (_age, _gender) {
            if (_gender == 'male')
                return _age + ' лет';
            else
                return 'Не так важно';
        }
    }

    /* ageFilter */

    function orderKeyNameFilter() {
        return function (_orderKey) {
            switch (_orderKey) {
                case 'name':
                    return "Имени";
                case 'age':
                    return "Возрасту";
                case 'gender':
                    return "Полу";
                case 'eyeColor':
                    return 'Цвету глаз';
            }
        }
    }

    /* orderKeyNameFilter */

    function usersFactory() {
        var o = {};


        return o;
    }

    /* usersFactory */

    // function UsersController($log, FURL, user, fakeconst, usersServ, usersFact, pusers) {
    function UsersController($log, users, $filter) {
        $log.init('UsersCtrl');
        var s = this;
        // $log.debug('UsersCtrl ', FURL);
        // $log.debug('UsersCtrl ', user);
        // $log.debug('UsersCtrl ', fakeconst);
        // $log.debug('UsersCtrl ', usersServ.get());
        // $log.debug('UsersCtrl ', usersFact.get());
        // $log.debug('UsersCtrl ', pusers.id);
        // $log.debug('UsersCtrl ', pusers.getConfig());
        // s.arrayExample = [1,2,3,3,4];

        s.usersArray = [
            {
                "_id": "5708c5f0970230ab894e8318",
                "index": 0,
                "age": 33,
                "eyeColor": "зеленые",
                "name": "Noemi Barnett",
                "gender": "female"
            },
            {
                "_id": "5708c5f04f16609d42af0b03",
                "index": 1,
                "age": 26,
                "eyeColor": "голубые",
                "name": "Cash Mcgowan",
                "gender": "male"
            },
            {
                "_id": "5708c5f01267012cb935fc86",
                "index": 2,
                "age": 20,
                "eyeColor": "зеленые",
                "name": "Nellie Ayers",
                "gender": "female"
            },
            {
                "_id": "5708c5f0233b4357a2d99bec",
                "index": 3,
                "age": 33,
                "eyeColor": "голубые",
                "name": "Oneal Fitzgerald",
                "gender": "male"
            },
            {
                "_id": "5708c5f0d4771b06a6633b85",
                "index": 4,
                "age": 26,
                "eyeColor": "карие",
                "name": "Hendricks Kirkland",
                "gender": "male"
            },
            {
                "_id": "5708c5f030f0d09538c9d503",
                "index": 5,
                "age": 23,
                "eyeColor": "зеленые",
                "name": "Atkinson Chavez",
                "gender": "male"
            },
            {
                "_id": "5708c5f0d85ed7f0bed70cf9",
                "index": 6,
                "age": 37,
                "eyeColor": "голубые",
                "name": "Noel Nelson",
                "gender": "male"
            },
            {
                "_id": "5708c5f03322ca503174d124",
                "index": 7,
                "age": 35,
                "eyeColor": "карие",
                "name": "Holloway Suarez",
                "gender": "male"
            },
            {
                "_id": "5708c5f0f245878bbe5b1d05",
                "index": 8,
                "age": 36,
                "eyeColor": "голубые",
                "name": "Marcy Mcbride",
                "gender": "female"
            },
            {
                "_id": "5708c5f03d000c4cd294079a",
                "index": 9,
                "age": 24,
                "eyeColor": "голубые",
                "name": "Patrica Randolph",
                "gender": "female"
            },
            {
                "_id": "5708c5f0ffb7acc9099188b5",
                "index": 10,
                "age": 24,
                "eyeColor": "голубые",
                "name": "Josefa Huff",
                "gender": "female"
            },
            {
                "_id": "5708c5f0840c5bdc17c324f7",
                "index": 11,
                "age": 22,
                "eyeColor": "карие",
                "name": "Delaney Noel",
                "gender": "male"
            },
            {
                "_id": "5708c5f0b25acf4a6ead0818",
                "index": 12,
                "age": 34,
                "eyeColor": "карие",
                "name": "Megan Mccullough",
                "gender": "female"
            },
            {
                "_id": "5708c5f02c47346ac8240339",
                "index": 13,
                "age": 40,
                "eyeColor": "голубые",
                "name": "Oneill Ferguson",
                "gender": "male"
            },
            {
                "_id": "5708c5f0807bf353237b8a5a",
                "index": 14,
                "age": 21,
                "eyeColor": "голубые",
                "name": "Church Snider",
                "gender": "male"
            },
            {
                "_id": "5708c5f03cdb80deace08028",
                "index": 15,
                "age": 33,
                "eyeColor": "карие",
                "name": "Hartman Emerson",
                "gender": "male"
            },
            {
                "_id": "5708c5f0ed4c84eaf05298d7",
                "index": 16,
                "age": 32,
                "eyeColor": "карие",
                "name": "Erin Hunter",
                "gender": "female"
            },
            {
                "_id": "5708c5f030ff53dfd77570ac",
                "index": 17,
                "age": 22,
                "eyeColor": "голубые",
                "name": "Mejia Diaz",
                "gender": "male"
            },
            {
                "_id": "5708c5f0140a64e7baaf442d",
                "index": 18,
                "age": 25,
                "eyeColor": "карие",
                "name": "Lacy Fletcher",
                "gender": "female"
            },
            {
                "_id": "5708c5f0078ed72657f6aca6",
                "index": 19,
                "age": 32,
                "eyeColor": "зеленые",
                "name": "Chasity Serrano",
                "gender": "female"
            },
            {
                "_id": "5708c5f012c24527770adfc3",
                "index": 20,
                "age": 32,
                "eyeColor": "карие",
                "name": "Mcgee Manning",
                "gender": "male"
            },
            {
                "_id": "5708c5f03cf1ca93bc9cf1d9",
                "index": 21,
                "age": 33,
                "eyeColor": "зеленые",
                "name": "Sara Benson",
                "gender": "female"
            },
            {
                "_id": "5708c5f04f3b72f1c323ab81",
                "index": 22,
                "age": 38,
                "eyeColor": "карие",
                "name": "Dillon Hess",
                "gender": "male"
            },
            {
                "_id": "5708c5f0e57f4f4c3e9542fc",
                "index": 23,
                "age": 25,
                "eyeColor": "зеленые",
                "name": "Harrison Stokes",
                "gender": "male"
            },
            {
                "_id": "5708c5f03572dd1e43f1fa17",
                "index": 24,
                "age": 24,
                "eyeColor": "карие",
                "name": "Gould Schultz",
                "gender": "male"
            },
            {
                "_id": "5708c5f01ea51d92ba60ee57",
                "index": 25,
                "age": 23,
                "eyeColor": "голубые",
                "name": "Trina Ryan",
                "gender": "female"
            },
            {
                "_id": "5708c5f08ba0f26dc82e1e65",
                "index": 26,
                "age": 40,
                "eyeColor": "голубые",
                "name": "Clemons Murray",
                "gender": "male"
            },
            {
                "_id": "5708c5f0f57bbce1571f10f6",
                "index": 27,
                "age": 34,
                "eyeColor": "голубые",
                "name": "Nannie Vargas",
                "gender": "female"
            },
            {
                "_id": "5708c5f05bd7f643cde54b81",
                "index": 28,
                "age": 27,
                "eyeColor": "голубые",
                "name": "Barton Glover",
                "gender": "male"
            },
            {
                "_id": "5708c5f0df6db8e5486dcd33",
                "index": 29,
                "age": 28,
                "eyeColor": "голубые",
                "name": "Katrina Cox",
                "gender": "female"
            },
            {
                "_id": "5708c5f0d750a7d748b18870",
                "index": 30,
                "age": 29,
                "eyeColor": "зеленые",
                "name": "Nixon Grimes",
                "gender": "male"
            },
            {
                "_id": "5708c5f06b2ce497ceedae13",
                "index": 31,
                "age": 38,
                "eyeColor": "голубые",
                "name": "Ruby Cooke",
                "gender": "female"
            },
            {
                "_id": "5708c5f06baa6545db4b674b",
                "index": 32,
                "age": 28,
                "eyeColor": "карие",
                "name": "Meyers Hurley",
                "gender": "male"
            },
            {
                "_id": "5708c5f01fa203a3aaa3ac6b",
                "index": 33,
                "age": 38,
                "eyeColor": "карие",
                "name": "Graciela Garner",
                "gender": "female"
            },
            {
                "_id": "5708c5f0c58e288120b51ec6",
                "index": 34,
                "age": 20,
                "eyeColor": "голубые",
                "name": "Reese Rivera",
                "gender": "male"
            },
            {
                "_id": "5708c5f0b13082654e89c454",
                "index": 35,
                "age": 32,
                "eyeColor": "карие",
                "name": "Webster Frazier",
                "gender": "male"
            },
            {
                "_id": "5708c5f04458f35887f3c8f6",
                "index": 36,
                "age": 21,
                "eyeColor": "голубые",
                "name": "Vicky Bradley",
                "gender": "female"
            },
            {
                "_id": "5708c5f065f9c4a6f9525be2",
                "index": 37,
                "age": 33,
                "eyeColor": "голубые",
                "name": "Sellers Reilly",
                "gender": "male"
            },
            {
                "_id": "5708c5f004ffd9f0d1fdfb65",
                "index": 38,
                "age": 24,
                "eyeColor": "голубые",
                "name": "Wiggins Weeks",
                "gender": "male"
            },
            {
                "_id": "5708c5f0b2f93fbe25b33134",
                "index": 39,
                "age": 26,
                "eyeColor": "зеленые",
                "name": "Jenifer Snyder",
                "gender": "female"
            },
            {
                "_id": "5708c5f01f79ef908172983e",
                "index": 40,
                "age": 33,
                "eyeColor": "карие",
                "name": "Lottie Byers",
                "gender": "female"
            },
            {
                "_id": "5708c5f00cd59546e15779ec",
                "index": 41,
                "age": 36,
                "eyeColor": "голубые",
                "name": "Hoover Thomas",
                "gender": "male"
            },
            {
                "_id": "5708c5f05bc11c56f3888185",
                "index": 42,
                "age": 26,
                "eyeColor": "голубые",
                "name": "Katina Fuentes",
                "gender": "female"
            },
            {
                "_id": "5708c5f051cfd5c176aa97c5",
                "index": 43,
                "age": 30,
                "eyeColor": "голубые",
                "name": "Chambers Henry",
                "gender": "male"
            },
            {
                "_id": "5708c5f043af153c9fca04cd",
                "index": 44,
                "age": 20,
                "eyeColor": "зеленые",
                "name": "Beck Conway",
                "gender": "male"
            },
            {
                "_id": "5708c5f0e93a10f918351a26",
                "index": 45,
                "age": 35,
                "eyeColor": "зеленые",
                "name": "Jerri Gilliam",
                "gender": "female"
            },
            {
                "_id": "5708c5f002461f39fd6a38b9",
                "index": 46,
                "age": 26,
                "eyeColor": "голубые",
                "name": "Consuelo Robbins",
                "gender": "female"
            },
            {
                "_id": "5708c5f00aeadc115fbc87dd",
                "index": 47,
                "age": 40,
                "eyeColor": "карие",
                "name": "Dennis Navarro",
                "gender": "male"
            },
            {
                "_id": "5708c5f08caf2cfcd81937f7",
                "index": 48,
                "age": 20,
                "eyeColor": "голубые",
                "name": "Gay Parrish",
                "gender": "female"
            },
            {
                "_id": "5708c5f074cad52d335d9aae",
                "index": 49,
                "age": 38,
                "eyeColor": "голубые",
                "name": "Ava Head",
                "gender": "female"
            },
            {
                "_id": "5708c5f0b2720ef4bc201cf7",
                "index": 50,
                "age": 28,
                "eyeColor": "карие",
                "name": "Shawna Tanner",
                "gender": "female"
            },
            {
                "_id": "5708c5f058625b738ce285fd",
                "index": 51,
                "age": 35,
                "eyeColor": "голубые",
                "name": "Robbie Wilkinson",
                "gender": "female"
            },
            {
                "_id": "5708c5f04cac574442e315b0",
                "index": 52,
                "age": 35,
                "eyeColor": "голубые",
                "name": "Glass Andrews",
                "gender": "male"
            },
            {
                "_id": "5708c5f04617f5e8c94da323",
                "index": 53,
                "age": 25,
                "eyeColor": "карие",
                "name": "Cabrera Shields",
                "gender": "male"
            },
            {
                "_id": "5708c5f08016d038fe8d759b",
                "index": 54,
                "age": 22,
                "eyeColor": "голубые",
                "name": "Wright Kelly",
                "gender": "male"
            },
            {
                "_id": "5708c5f05c82cbe1f0406f42",
                "index": 55,
                "age": 30,
                "eyeColor": "голубые",
                "name": "Mcknight Montoya",
                "gender": "male"
            },
            {
                "_id": "5708c5f0b14dad0bea5f270c",
                "index": 56,
                "age": 30,
                "eyeColor": "зеленые",
                "name": "Whitley Cochran",
                "gender": "male"
            },
            {
                "_id": "5708c5f08e259c59c3f71817",
                "index": 57,
                "age": 25,
                "eyeColor": "карие",
                "name": "Rosalind Mueller",
                "gender": "female"
            },
            {
                "_id": "5708c5f08a82900a0a667acb",
                "index": 58,
                "age": 37,
                "eyeColor": "карие",
                "name": "Sally Ellison",
                "gender": "female"
            },
            {
                "_id": "5708c5f034f39a74bad32e23",
                "index": 59,
                "age": 25,
                "eyeColor": "зеленые",
                "name": "Colon Green",
                "gender": "male"
            },
            {
                "_id": "5708c5f066b4da28ae564c96",
                "index": 60,
                "age": 35,
                "eyeColor": "зеленые",
                "name": "Meyer David",
                "gender": "male"
            },
            {
                "_id": "5708c5f0426a7d1afe127ac3",
                "index": 61,
                "age": 25,
                "eyeColor": "голубые",
                "name": "Kathleen Jensen",
                "gender": "female"
            },
            {
                "_id": "5708c5f0abc23df73e616161",
                "index": 62,
                "age": 32,
                "eyeColor": "голубые",
                "name": "Cindy Mclaughlin",
                "gender": "female"
            },
            {
                "_id": "5708c5f0924c30796fe2560c",
                "index": 63,
                "age": 39,
                "eyeColor": "карие",
                "name": "Jackie Allen",
                "gender": "female"
            },
            {
                "_id": "5708c5f07e83a6fd1c70c052",
                "index": 64,
                "age": 27,
                "eyeColor": "карие",
                "name": "Linda Langley",
                "gender": "female"
            },
            {
                "_id": "5708c5f0195c55780787c10a",
                "index": 65,
                "age": 39,
                "eyeColor": "зеленые",
                "name": "Acevedo Hodges",
                "gender": "male"
            },
            {
                "_id": "5708c5f0a6191ff7e13d7730",
                "index": 66,
                "age": 29,
                "eyeColor": "карие",
                "name": "Day Lyons",
                "gender": "male"
            },
            {
                "_id": "5708c5f04043da0ce24037f5",
                "index": 67,
                "age": 30,
                "eyeColor": "карие",
                "name": "Herrera Sawyer",
                "gender": "male"
            },
            {
                "_id": "5708c5f039028f2806afd85a",
                "index": 68,
                "age": 25,
                "eyeColor": "зеленые",
                "name": "Arline Meyers",
                "gender": "female"
            },
            {
                "_id": "5708c5f0ddea8d7dc1637ebf",
                "index": 69,
                "age": 34,
                "eyeColor": "зеленые",
                "name": "Marisa Stuart",
                "gender": "female"
            },
            {
                "_id": "5708c5f0fd088b173889ef9e",
                "index": 70,
                "age": 31,
                "eyeColor": "голубые",
                "name": "Lenore King",
                "gender": "female"
            },
            {
                "_id": "5708c5f0df3a9d188006d7a2",
                "index": 71,
                "age": 34,
                "eyeColor": "зеленые",
                "name": "Sherri Nash",
                "gender": "female"
            },
            {
                "_id": "5708c5f026746cf7447f759d",
                "index": 72,
                "age": 39,
                "eyeColor": "голубые",
                "name": "Mckay Herring",
                "gender": "male"
            },
            {
                "_id": "5708c5f0f671f344838b08fc",
                "index": 73,
                "age": 30,
                "eyeColor": "карие",
                "name": "Ida Ruiz",
                "gender": "female"
            },
            {
                "_id": "5708c5f00cf00586c67bf845",
                "index": 74,
                "age": 40,
                "eyeColor": "голубые",
                "name": "Cathleen Roberts",
                "gender": "female"
            },
            {
                "_id": "5708c5f04d457f9ad7e1bf78",
                "index": 75,
                "age": 33,
                "eyeColor": "зеленые",
                "name": "Chrystal Gates",
                "gender": "female"
            },
            {
                "_id": "5708c5f03dd1bc442a9de57c",
                "index": 76,
                "age": 36,
                "eyeColor": "карие",
                "name": "Short Bradshaw",
                "gender": "male"
            },
            {
                "_id": "5708c5f08a81613bf30553ac",
                "index": 77,
                "age": 21,
                "eyeColor": "карие",
                "name": "Estrada Solis",
                "gender": "male"
            },
            {
                "_id": "5708c5f02ee9907f556f4c4a",
                "index": 78,
                "age": 38,
                "eyeColor": "зеленые",
                "name": "Finley Brady",
                "gender": "male"
            },
            {
                "_id": "5708c5f082e6f6092ffe129e",
                "index": 79,
                "age": 25,
                "eyeColor": "зеленые",
                "name": "Drake Mccray",
                "gender": "male"
            },
            {
                "_id": "5708c5f0b1bed4c9fa22aa48",
                "index": 80,
                "age": 27,
                "eyeColor": "карие",
                "name": "Alyson Duffy",
                "gender": "female"
            },
            {
                "_id": "5708c5f0cef9f348bd61308a",
                "index": 81,
                "age": 37,
                "eyeColor": "голубые",
                "name": "Ann Martinez",
                "gender": "female"
            },
            {
                "_id": "5708c5f086353c7886f98dff",
                "index": 82,
                "age": 39,
                "eyeColor": "голубые",
                "name": "York Byrd",
                "gender": "male"
            },
            {
                "_id": "5708c5f042073cf7ede0cd86",
                "index": 83,
                "age": 34,
                "eyeColor": "зеленые",
                "name": "Minnie Carney",
                "gender": "female"
            },
            {
                "_id": "5708c5f0c09c45a6b57ff2ec",
                "index": 84,
                "age": 39,
                "eyeColor": "карие",
                "name": "Marquez Kerr",
                "gender": "male"
            },
            {
                "_id": "5708c5f087e726a7e6091e15",
                "index": 85,
                "age": 40,
                "eyeColor": "зеленые",
                "name": "Debra Copeland",
                "gender": "female"
            },
            {
                "_id": "5708c5f0d17143c05cbd9d03",
                "index": 86,
                "age": 29,
                "eyeColor": "зеленые",
                "name": "Baxter Farrell",
                "gender": "male"
            },
            {
                "_id": "5708c5f0bb0d54fee84607b1",
                "index": 87,
                "age": 25,
                "eyeColor": "карие",
                "name": "Dillard Pace",
                "gender": "male"
            },
            {
                "_id": "5708c5f052f41f0e13790598",
                "index": 88,
                "age": 22,
                "eyeColor": "голубые",
                "name": "Kimberley Bernard",
                "gender": "female"
            },
            {
                "_id": "5708c5f07ba66e93e398ccf9",
                "index": 89,
                "age": 29,
                "eyeColor": "карие",
                "name": "Ada Roman",
                "gender": "female"
            },
            {
                "_id": "5708c5f0045accefc21f32be",
                "index": 90,
                "age": 33,
                "eyeColor": "голубые",
                "name": "Janis Ochoa",
                "gender": "female"
            },
            {
                "_id": "5708c5f0c5b80ecc953601d6",
                "index": 91,
                "age": 35,
                "eyeColor": "карие",
                "name": "Blanchard Curry",
                "gender": "male"
            },
            {
                "_id": "5708c5f0ffefc53e85944d0d",
                "index": 92,
                "age": 39,
                "eyeColor": "голубые",
                "name": "Estes Park",
                "gender": "male"
            },
            {
                "_id": "5708c5f005ae2113b0ae885f",
                "index": 93,
                "age": 38,
                "eyeColor": "карие",
                "name": "Mandy Crawford",
                "gender": "female"
            },
            {
                "_id": "5708c5f07af654c325e5b975",
                "index": 94,
                "age": 31,
                "eyeColor": "зеленые",
                "name": "Dixon Leach",
                "gender": "male"
            },
            {
                "_id": "5708c5f0903f89828117c71a",
                "index": 95,
                "age": 31,
                "eyeColor": "карие",
                "name": "Tonya Stanley",
                "gender": "female"
            },
            {
                "_id": "5708c5f04cbdbfb2ae72e7da",
                "index": 96,
                "age": 35,
                "eyeColor": "карие",
                "name": "Bryant Harvey",
                "gender": "male"
            },
            {
                "_id": "5708c5f0ae9803f0256e4baf",
                "index": 97,
                "age": 31,
                "eyeColor": "голубые",
                "name": "Rogers Melton",
                "gender": "male"
            },
            {
                "_id": "5708c5f029660192033ccf0e",
                "index": 98,
                "age": 32,
                "eyeColor": "карие",
                "name": "Ophelia Abbott",
                "gender": "female"
            },
            {
                "_id": "5708c5f05fbc9a08d27400df",
                "index": 99,
                "age": 35,
                "eyeColor": "зеленые",
                "name": "Tania Barton",
                "gender": "female"
            },
            {
                "_id": "5708c5f0e7567df765cc1d5f",
                "index": 100,
                "age": 24,
                "eyeColor": "зеленые",
                "name": "Lawrence Lindsay",
                "gender": "male"
            },
            {
                "_id": "5708c5f05be3c827536b57ca",
                "index": 101,
                "age": 25,
                "eyeColor": "голубые",
                "name": "Jones Guzman",
                "gender": "male"
            },
            {
                "_id": "5708c5f01f3815bc43f7b1ef",
                "index": 102,
                "age": 37,
                "eyeColor": "карие",
                "name": "Imogene Floyd",
                "gender": "female"
            },
            {
                "_id": "5708c5f0d788c1ce95de326b",
                "index": 103,
                "age": 28,
                "eyeColor": "голубые",
                "name": "Bessie Watts",
                "gender": "female"
            },
            {
                "_id": "5708c5f0eaaa12922421f349",
                "index": 104,
                "age": 30,
                "eyeColor": "голубые",
                "name": "Lilly Salazar",
                "gender": "female"
            },
            {
                "_id": "5708c5f07de2a4e27f84071c",
                "index": 105,
                "age": 23,
                "eyeColor": "зеленые",
                "name": "Terri Burch",
                "gender": "female"
            },
            {
                "_id": "5708c5f06d280a5a8637645c",
                "index": 106,
                "age": 38,
                "eyeColor": "карие",
                "name": "Sanchez Vasquez",
                "gender": "male"
            },
            {
                "_id": "5708c5f09af43c9e32cfa00c",
                "index": 107,
                "age": 22,
                "eyeColor": "голубые",
                "name": "Ryan Kinney",
                "gender": "male"
            },
            {
                "_id": "5708c5f0fcf08e31e2c7ea1e",
                "index": 108,
                "age": 40,
                "eyeColor": "зеленые",
                "name": "Taylor Heath",
                "gender": "female"
            },
            {
                "_id": "5708c5f081c279855f818286",
                "index": 109,
                "age": 34,
                "eyeColor": "зеленые",
                "name": "Todd Small",
                "gender": "male"
            },
            {
                "_id": "5708c5f0d66d4491c39958c3",
                "index": 110,
                "age": 33,
                "eyeColor": "голубые",
                "name": "Kemp Sharp",
                "gender": "male"
            },
            {
                "_id": "5708c5f0f68a11df39e07dd8",
                "index": 111,
                "age": 27,
                "eyeColor": "карие",
                "name": "Janine Huber",
                "gender": "female"
            },
            {
                "_id": "5708c5f0ba5f6f6ba4ad0210",
                "index": 112,
                "age": 24,
                "eyeColor": "зеленые",
                "name": "Nguyen Juarez",
                "gender": "male"
            },
            {
                "_id": "5708c5f0c2ac3940bd79f29d",
                "index": 113,
                "age": 20,
                "eyeColor": "зеленые",
                "name": "Virgie Atkinson",
                "gender": "female"
            },
            {
                "_id": "5708c5f0361b13f907da9415",
                "index": 114,
                "age": 36,
                "eyeColor": "голубые",
                "name": "Myra Howe",
                "gender": "female"
            },
            {
                "_id": "5708c5f0399bcd39bb02d3f9",
                "index": 115,
                "age": 24,
                "eyeColor": "голубые",
                "name": "Caldwell Mooney",
                "gender": "male"
            },
            {
                "_id": "5708c5f0e8263ec227a539b4",
                "index": 116,
                "age": 20,
                "eyeColor": "голубые",
                "name": "Lowery Palmer",
                "gender": "male"
            },
            {
                "_id": "5708c5f06e59ff18871c8267",
                "index": 117,
                "age": 37,
                "eyeColor": "карие",
                "name": "Kane Hester",
                "gender": "male"
            },
            {
                "_id": "5708c5f0332d36354e7e82b4",
                "index": 118,
                "age": 37,
                "eyeColor": "голубые",
                "name": "Baird Pope",
                "gender": "male"
            },
            {
                "_id": "5708c5f076d694bdf441625d",
                "index": 119,
                "age": 28,
                "eyeColor": "голубые",
                "name": "Anderson Williams",
                "gender": "male"
            },
            {
                "_id": "5708c5f062b037c025eb5727",
                "index": 120,
                "age": 36,
                "eyeColor": "зеленые",
                "name": "Albert Tucker",
                "gender": "male"
            },
            {
                "_id": "5708c5f03d12ffc7f39e6538",
                "index": 121,
                "age": 31,
                "eyeColor": "карие",
                "name": "Marsh Miranda",
                "gender": "male"
            },
            {
                "_id": "5708c5f0534032468f033d35",
                "index": 122,
                "age": 28,
                "eyeColor": "голубые",
                "name": "Geraldine Bright",
                "gender": "female"
            },
            {
                "_id": "5708c5f047563b0da7db7acc",
                "index": 123,
                "age": 29,
                "eyeColor": "голубые",
                "name": "Gates Sears",
                "gender": "male"
            },
            {
                "_id": "5708c5f0a10cf897d0e97dab",
                "index": 124,
                "age": 26,
                "eyeColor": "карие",
                "name": "Rosemarie Chase",
                "gender": "female"
            },
            {
                "_id": "5708c5f0d9d202bc3e1e87a1",
                "index": 125,
                "age": 40,
                "eyeColor": "карие",
                "name": "Ball Clarke",
                "gender": "male"
            },
            {
                "_id": "5708c5f0a501315151fe0b6b",
                "index": 126,
                "age": 39,
                "eyeColor": "голубые",
                "name": "Hubbard Greene",
                "gender": "male"
            },
            {
                "_id": "5708c5f07fd061aa5b925274",
                "index": 127,
                "age": 20,
                "eyeColor": "зеленые",
                "name": "Jaime Roach",
                "gender": "female"
            },
            {
                "_id": "5708c5f054c3a7ab7cb5c7b2",
                "index": 128,
                "age": 26,
                "eyeColor": "голубые",
                "name": "Mays Best",
                "gender": "male"
            },
            {
                "_id": "5708c5f0df13f64c57e9a94f",
                "index": 129,
                "age": 33,
                "eyeColor": "карие",
                "name": "Duffy Hull",
                "gender": "male"
            },
            {
                "_id": "5708c5f0420c21bd94b78e1b",
                "index": 130,
                "age": 38,
                "eyeColor": "зеленые",
                "name": "Elma Oneal",
                "gender": "female"
            },
            {
                "_id": "5708c5f025a97a019b9cc992",
                "index": 131,
                "age": 35,
                "eyeColor": "карие",
                "name": "Angelique Pickett",
                "gender": "female"
            },
            {
                "_id": "5708c5f0e75307719056f099",
                "index": 132,
                "age": 37,
                "eyeColor": "карие",
                "name": "Bryan Albert",
                "gender": "male"
            },
            {
                "_id": "5708c5f08a28af370aefdb5d",
                "index": 133,
                "age": 24,
                "eyeColor": "зеленые",
                "name": "Porter Hayden",
                "gender": "male"
            },
            {
                "_id": "5708c5f0bb0a913e49df8a17",
                "index": 134,
                "age": 23,
                "eyeColor": "голубые",
                "name": "Goodwin Contreras",
                "gender": "male"
            },
            {
                "_id": "5708c5f0c3b61fd5c0bbb000",
                "index": 135,
                "age": 23,
                "eyeColor": "карие",
                "name": "Wooten Cruz",
                "gender": "male"
            },
            {
                "_id": "5708c5f04a2e0a90dad65846",
                "index": 136,
                "age": 40,
                "eyeColor": "голубые",
                "name": "Kinney Madden",
                "gender": "male"
            },
            {
                "_id": "5708c5f0c8c9fc3d8200a77d",
                "index": 137,
                "age": 20,
                "eyeColor": "зеленые",
                "name": "Michelle Watson",
                "gender": "female"
            },
            {
                "_id": "5708c5f0311abe52cb92bcf1",
                "index": 138,
                "age": 27,
                "eyeColor": "голубые",
                "name": "Farmer Townsend",
                "gender": "male"
            },
            {
                "_id": "5708c5f02f1e355bbca9648e",
                "index": 139,
                "age": 37,
                "eyeColor": "голубые",
                "name": "Lopez Bishop",
                "gender": "male"
            },
            {
                "_id": "5708c5f0e2725e9bc7e7b6ee",
                "index": 140,
                "age": 26,
                "eyeColor": "голубые",
                "name": "Gertrude Rivas",
                "gender": "female"
            },
            {
                "_id": "5708c5f09862c4491b28f5ab",
                "index": 141,
                "age": 37,
                "eyeColor": "зеленые",
                "name": "Scott Maldonado",
                "gender": "male"
            },
            {
                "_id": "5708c5f0e898261b75de4d53",
                "index": 142,
                "age": 40,
                "eyeColor": "карие",
                "name": "Dollie Charles",
                "gender": "female"
            },
            {
                "_id": "5708c5f0a0e094970300243f",
                "index": 143,
                "age": 35,
                "eyeColor": "карие",
                "name": "Mccarty Dale",
                "gender": "male"
            },
            {
                "_id": "5708c5f0e455d113dcab1462",
                "index": 144,
                "age": 27,
                "eyeColor": "голубые",
                "name": "Sweeney Hobbs",
                "gender": "male"
            },
            {
                "_id": "5708c5f0b35b525147cc24ba",
                "index": 145,
                "age": 40,
                "eyeColor": "карие",
                "name": "Dixie May",
                "gender": "female"
            },
            {
                "_id": "5708c5f00e895330f4649661",
                "index": 146,
                "age": 29,
                "eyeColor": "карие",
                "name": "Yvette Mejia",
                "gender": "female"
            },
            {
                "_id": "5708c5f0794d52cc2dca9f17",
                "index": 147,
                "age": 29,
                "eyeColor": "карие",
                "name": "Harrington Buck",
                "gender": "male"
            },
            {
                "_id": "5708c5f0091e54093ea62f65",
                "index": 148,
                "age": 33,
                "eyeColor": "голубые",
                "name": "Lyons Day",
                "gender": "male"
            },
            {
                "_id": "5708c5f0150a0b9ed430fb3e",
                "index": 149,
                "age": 22,
                "eyeColor": "зеленые",
                "name": "Delgado Olson",
                "gender": "male"
            },
            {
                "_id": "5708c5f092de2f70fb605fa6",
                "index": 150,
                "age": 38,
                "eyeColor": "голубые",
                "name": "Debora Livingston",
                "gender": "female"
            },
            {
                "_id": "5708c5f0a197659de52e45e9",
                "index": 151,
                "age": 28,
                "eyeColor": "голубые",
                "name": "Sargent Foreman",
                "gender": "male"
            },
            {
                "_id": "5708c5f0a9fe157ed7c7d3f0",
                "index": 152,
                "age": 32,
                "eyeColor": "голубые",
                "name": "Genevieve Lynch",
                "gender": "female"
            },
            {
                "_id": "5708c5f0a53c793d10e2f69f",
                "index": 153,
                "age": 39,
                "eyeColor": "карие",
                "name": "Arlene Wilcox",
                "gender": "female"
            },
            {
                "_id": "5708c5f01584e9387c4a2ac1",
                "index": 154,
                "age": 35,
                "eyeColor": "зеленые",
                "name": "Jodie Elliott",
                "gender": "female"
            },
            {
                "_id": "5708c5f0f2b3900e958a7eab",
                "index": 155,
                "age": 21,
                "eyeColor": "голубые",
                "name": "Golden Mullins",
                "gender": "male"
            },
            {
                "_id": "5708c5f0da2b3f7d301826c0",
                "index": 156,
                "age": 40,
                "eyeColor": "зеленые",
                "name": "Hodge Reyes",
                "gender": "male"
            }
        ]

        // s.usersArray = $filter('orderBy')(usersArray, 'name');

    }


    // function UsersConfig($stateProvider, pusersProvider, $provide)
    function UsersConfig($stateProvider, $provide) {
        $provide.decorator('$log', function ($delegate) {
            $delegate.init = function (moduleName) {
                $delegate.debug(moduleName + " init");
            }
            return $delegate;
        })

        // console.log('UsersConfig ', pusersProvider.getConfigVal());
        // pusersProvider.setConfigVal('Привет!');
        $stateProvider
            .state('users', {
                url: '/users',
                templateUrl: 'app/users/users.html',
                controller: 'UsersCtrl',
                controllerAs: 'uc'
            })
    }

})();
