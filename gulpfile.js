'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    scss = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber'),
    ngAnnotate = require('gulp-ng-annotate'),
    sourcemaps = require('gulp-sourcemaps'),
    webserver = require('gulp-webserver');

gulp.task('js',function(){
  gulp.src([
      'builds/dev/app/**/*.js',
      '!builds/dev/app/**/*_test.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(plumber())
    .pipe(ngAnnotate())
    // .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('builds/dev'));
})

gulp.task('jslibs', function(){
  gulp.src([
      'bower_components/angular/angular.js',
      'bower_components/angular-ui-router/release/angular-ui-router.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',

      // 'bower_components/angular-route/angular-route.js',
      // 'bower_components/angular-file-upload/dist/angular-file-upload.js',
      // 'bower_components/firebase/firebase-debug.js',
      // 'bower_components/angularfire/dist/angularfire.js'
    ])
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('builds/dev'));
  });

gulp.task('pjs',function(){
  gulp.src([
      'builds/dev/app/**/*.js',
      '!builds/dev/app/**/*_test.js'
    ])
    .pipe(concat('app.js'))
    .pipe(plumber())
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('builds/prod'));
  gulp.src([
      'bower_components/angular/angular.min.js',
      'bower_components/angular-ui-router/release/angular-ui-router.min.js',
      // 'bower_components/angular-route/angular-route.min.js',
      // 'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
    ])
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('builds/prod'));
})

gulp.task('css', function(){
  gulp.src('builds/dev/app/**/*.scss')
    .pipe(plumber())
    .pipe(scss())
    .pipe(concat('app.css'))
    .pipe(gulp.dest('builds/dev'));
})

gulp.task('csslibs', function(){
  gulp.src([
      'bower_components/angular/angular-csp.css',
      'bower_components/bootstrap/dist/css/bootstrap.css',
      'bower_components/angular-bootstrap/ui-bootstrap-csp.css',
      //'bower_components/font-awesome/css/font-awesome.min.css'
      // 'bower_components/bootstrap/dist/css/bootstrap.css',
    ])
    .pipe(concat('theme.css'))
    .pipe(gulp.dest('builds/dev'));
})

gulp.task('pcss', function(){
  gulp.src('builds/dev/app/**/*.scss')
    .pipe(plumber())
    .pipe(scss())
    .pipe(concat('app.css'))
    .pipe(uglify())
    .pipe(gulp.dest('builds/prod'));
  gulp.src([
      'bower_components/angular/angular-csp.css',
      'bower_components/angular-bootstrap/ui-bootstrap-csp.css',
      'bower_components/bootstrap/dist/css/bootstrap.min.css',
      //'bower_components/font-awesome/css/font-awesome.min.css'
    ])
    .pipe(concat('theme.css'))
    .pipe(gulp.dest('builds/prod'));
})

gulp.task('phtml', function(){
  gulp.src('builds/dev/**/*.html')
    .pipe(gulp.dest('builds/prod'));
})

gulp.task('watch', function(){
  gulp.watch('builds/dev/app/**/*.js', ['js']);
  gulp.watch('builds/dev/app/**/*.scss', ['css']);
})

gulp.task('pwatch', function(){
  gulp.watch('builds/dev/app/**/*.js', ['pjs']);
  gulp.watch('builds/dev/app/**/*.scss', ['pcss']);
  gulp.watch('builds/dev/**/*.html', ['phtml']);
})

gulp.task('webserver', function(){
  gulp.src('builds/dev')
    .pipe(webserver({
      livereload: true,
      open: true,
      port: 8034,
    }));
})

gulp.task('pwebserver', function(){
  gulp.src('builds/prod')
    .pipe(webserver({
      livereload: true,
      open: true,
      port: 8035,
    }));
})

gulp.task('default', [
    'jslibs',
    'js',
    'csslibs',
    'css',
    'watch',
    'webserver'
  ]);

gulp.task('prod', [
    'pjs', 
    'pcss',
    'phtml',
    'pwatch',
    'pwebserver'
  ]);